# Variable discovery

# TBB example generation
- Auxiliaries that remember just a few elements of the beginning/end of the array chunk are not correctly
handled.
- Parallel assignments problem.
# Proof generation
- Parallel assignments problem: need to figure out in which order the lemmas have to appear.