open Cil
open Format
open SPretty
open SketchTypes
open TestUtils
open Utils

(** Hashtable string -> 'a *)

let rec hash_str s =
  if String.length s > 0 then
    (int_of_char (String.get s 0)) +
    (hash_str (String.sub s 1 ((String.length s)- 1))) * 10
  else
    0

module SH = Hashtbl.Make
    (struct type t = string let compare = String.compare
      let equal s s' = s = s'
      let hash s = hash_str s
    end)

(** List different types of operators:
    - comparison operators
    - associative operators
    - commutative operators

    And properties:
    - left and right distributivity.
*)

let comparison_operators : symb_binop list = [Gt; Ge; Lt; Le]
let associative_operators = [And; Or; Plus; Max; Min; Times]
let commutative_operators = [And; Or; Plus; Max; Min; Times; Eq; Neq]

let is_associative op = List.mem op associative_operators
let is_commutative op = List.mem op commutative_operators

let is_right_distributive op1 op2 =
  (** (a op1 b) op2 c = (a op2 c) op1 (b op2 c) *)
  match op1, op2 with
  | Or, And
  | Min, Plus
  | Max, Plus
  | Plus, Times -> true
  | _, _ ->  false

let is_left_distributive op1 op2 =
  (**  a op2 (b op1 c) = (a op1 b) op2 (a op1 c) *)
  match op1, op2 with
  | Plus, Times
  | Max, Plus
  | Min, Plus
  | Or, And -> true
  | _ , _ -> false

(** Operator -> variable *)
let c_t_int = TInt (IInt, [])
let c_t_bool = TInt (IBool, [])

let named_AC_ops = SH.create 10;;
(** Initialize map *)
SH.add named_AC_ops "plus"
  (makeVarinfo false "plus"
     (TFun (c_t_int,
            Some [("x", c_t_int, []); ("y", c_t_int, [])], false, [])));

SH.add named_AC_ops "times"
  (makeVarinfo false "times"
     (TFun (c_t_int,
            Some [("x", c_t_int, []); ("y", c_t_int, [])], false, [])));

SH.add named_AC_ops "max"
  (makeVarinfo false "max"
     (TFun (c_t_int,
            Some [("x", c_t_int, []); ("y", c_t_int, [])], false, [])));

SH.add named_AC_ops "min"
  (makeVarinfo false "min"
     (TFun (c_t_int,
            Some [("x", c_t_int, []); ("y", c_t_int, [])], false, [])));

SH.add named_AC_ops "and"
  (makeVarinfo false "and"
     (TFun (c_t_bool,
            Some [("x", c_t_bool, []); ("y", c_t_bool, [])], false, [])));

SH.add named_AC_ops "or"
  (makeVarinfo false "or"
     (TFun (c_t_bool,
            Some [("x", c_t_bool, []); ("y", c_t_bool, [])], false, [])));;

let get_AC_op op =
  let fsh = SH.find named_AC_ops in
  match op with
  | Plus -> fsh "plus"
  | Times -> fsh "times"
  | Max -> fsh "max"
  | Min -> fsh "min"
  | And -> fsh "and"
  | Or -> fsh "or"
  | _ -> raise Not_found

let op_from_name name =
  match name with
  | "plus" -> Plus
  | "times" -> Times
  | "and" -> And
  | "or" -> Or
  | "min" -> Min
  | "max" -> Max
  | _ -> raise Not_found


(** Identity rules *)
let operators_with_identities : symb_binop list = [Minus; Plus; Times; Div]

let apply_right_identity op t e =
  if t = Integer || t = Real || t = Num then (_b e op sk_zero) else e
  (*   match op with *)
  (*   | Plus | Minus -> _b e op sk_zero *)
  (*   | Times | Div -> _b e op sk_one *)
  (*   | _ -> e *)
  (* else e *)
let apply_left_identity op t e = e
  (* if t = Integer || t = Real || t = Num then *)
  (*   match op with *)
  (*   | Plus -> _b sk_zero op e *)
  (*   | Times | Div -> _b sk_one op e *)
  (*   | _ -> e *)
  (* else e *)
(** Flatten trees with a AC spine. Expressions are encoded via function
    applications : the function is named after the operator in the stringhash
    above.
*)
let rec flatten_AC expr =
  let rec flatten cur_op expr =
    match expr with
    | SkBinop (op, e1, e2) when op = cur_op ->
      (flatten cur_op e1)@(flatten cur_op e2)
    | e -> [flatten_AC e]
  in
  let filter_exprs e =
      match e with
        | SkBinop (op, e1, e2)
          when is_associative op && is_commutative op -> true
        | _ -> false
  in
  let flatten_expr rfunc e =
    match e with
    | SkBinop (op, e1, e2) when is_associative op && is_commutative op ->
      let operands = (flatten op e1)@(flatten op e2) in
      assert (List.length operands > 0);
      let func_vi = get_AC_op op in
      SkApp (Integer, Some func_vi, operands)
    | _ -> rfunc e
  in
  transform_expr filter_exprs flatten_expr identity identity expr



(** Equality under associativity and commutativity. Can be defined
    as structural equality of expressions trees with reordering in flat
    terms *)
let ( @= ) e1 e2 =
  let rec aux_eq e1 e2=
  match e1, e2 with
  | SkBinop (op1, e11, e12), SkBinop (op2, e21, e22) ->
    let strict_equal = op1 = op2 && aux_eq e11 e21 && aux_eq e12 e22 in
    let comm_eq = op1 = op2 && is_commutative op1 &&
                  aux_eq e11 e22 && aux_eq e12 e21
    in
    strict_equal || comm_eq

  | SkUnop (op1, e11), SkUnop (op2, e21) ->
    op1 = op2 && aux_eq e11 e21

  | SkApp (t1, Some v1, el1), SkApp (t2, Some v2, el2) ->
    if v1 = v2 then
      try
        let op = op_from_name v1.vname in
        is_commutative op &&
        List.length el1 = List.length el2 &&
        (List.for_all
           (fun elt1 ->
              List.exists (fun elt2 -> aux_eq elt1 elt2) el2)
           el1)
        &&
        (List.for_all
           (fun elt2 ->
              List.exists (fun elt1 -> aux_eq elt1 elt2) el1)
           el2)

      with Not_found ->
        el1 = el2
    else
      false

  | SkQuestion (c1, e11, e12), SkQuestion (c2, e21, e22) ->
    aux_eq c1 c2 && aux_eq e11 e21 && aux_eq e12 e22

  | _, _ -> e1 = e2
  in
  aux_eq (flatten_AC e1) (flatten_AC e2)


type ecost =
  {
    occurrences : int;
    max_depth : int;
  }

let expression_cost ctx e =
  let case0 = { occurrences = 0; max_depth = 0 } in
  let case1 = { occurrences = 1; max_depth = 1 } in
  let join_c c1 c2 =
    { occurrences = c1.occurrences + c2.occurrences;
      max_depth = let mx =  max c1.max_depth c2.max_depth in
        if mx > 0 then mx + 1 else 0 }
  in
  let special_case e = ES.mem e ctx.costly_exprs in
  let handle_spec f e =  case1 in
  let case_var v =
    let vi_o = vi_of v in
    match vi_o with
    | Some vi -> if VS.mem vi ctx.state_vars then case1 else case0
    | _ -> case0
  in
  let case_const c = case0 in
  rec_expr join_c case0 special_case handle_spec case_const case_var e

let ccost (d1, o1) (d2, o2) =
  if d1 = d2 then compare o1 o2 else compare d1 d2

let compare_ecost ec1 ec2 =
  ccost (ec1.max_depth, ec1.occurrences) (ec2.max_depth, ec2.occurrences)

let compare_cost ctx e1 e2 =
  compare_ecost (expression_cost ctx e1) (expression_cost ctx e2)

(** Compute the 'cost' of an expression with respect to a set of other
    c-expressions : the cost is the pair of the maximum depth of a
    c-expression in the expressions and the number of c-expressions in
    the expressions.
    @param stv the set of state variables.
    @param expr the expression of which we need to compute the cost.
    @return a pair of ints, the first element is the maximum depth of
    c-expression in the expression abstract syntax tree and the second element
    is the number of occurrences of c-expressions.
*)
let rec depth_cost ctx expr =
  let cost = expression_cost ctx expr in
  (cost.max_depth, cost.occurrences)

and depth_c_func ctx func =
  match func with
  | SkLetIn (velist, l') ->
    let dl', cl' = depth_c_func ctx l' in
    let max_de, sum_c =
      (List.fold_left
         (fun (mde, sec) (de, ec) -> (max mde de, sec + ec))
         (0, 0)
         (List.map (fun (v, e) -> depth_cost ctx e) velist)) in
    ((max max_de (if dl' > 0 then dl' + 1 else 0)), sum_c + cl')
  | SkLetExpr velist ->
    (List.fold_left
       (fun (mde, sec) (de, ec) -> (max mde de, sec + ec))
       (0, 0)
       (List.map (fun (v, e) -> depth_cost ctx e) velist))


let cost ctx expr =
  depth_cost ctx expr




(* AC rules *)
(* Rebuild an expression from its flattened expression. *)
let rec rebuild_tree_AC ctx =
  let rebuild_case expr =
    match expr with
    | SkApp (t, Some f, el) ->
      SH.mem named_AC_ops f.vname
    | _ -> false
  in
  let rebuild_flat_expr rfunc e =
    match e with
    | SkApp (t, Some f, el) ->
      begin
        let op = op_from_name f.vname in
        let el' = List.map rfunc el in
        let el_ordered =
             (List.sort (compare_cost ctx) el')
        in
        match el_ordered with
        | hd :: tl ->
          List.fold_left
            (fun tree e -> SkBinop (op, e, tree))
            hd tl
        | [] -> failwith "Unexpected length for list in AC conversion"
      end

    | _ -> failwith "Rebuild_flat_expr : Unexpected case."
  in
  transform_expr rebuild_case rebuild_flat_expr identity identity


(** Special rules, tranformations. *)
(** Inverse distributivity / factorization.
    This step rebuilds expression trees, but has to flatten the expressions
    it returns *)
let extract_operand_lists el =
  List.fold_left
    (fun (l1, l2) e ->
       match e with
       | SkBinop (_, e1, e2) -> (l1@[e1], l2@[e2])
       | _ -> l1, l2)
    ([], []) el

let __factorize__ ctx top_op el =
  let el = List.map (rebuild_tree_AC ctx) el in
  let el_binops, el_no_binops =
    List.partition (function | SkVar v -> true
                             | SkBinop (_, _, _) -> true
                             | _ -> false) el
  in
  let rec regroup el =
    match el with
    | hd :: tl ->
      begin (** begin match list *)
        match hd with
        | SkBinop (op, e1, e2) ->
          let ce1 = cost ctx e1 in
          let ce2 = cost ctx e2 in

          if is_left_distributive top_op op && ce1 > ce2 then
            (** Look for similar expressions in the list *)
            let sim_exprs, rtl =
              List.partition
                (fun e ->
                   match e with
                   | SkBinop (op', e1', ee) when op' = op && e1' @= e1 ->
                     true
                   | _ -> false) tl
            in
            let _, sim_exprs_snd = extract_operand_lists sim_exprs in
            let new_exprs =
              SkBinop
                (op,
                 e1,
                 SkApp (type_of e2, Some (get_AC_op top_op), e2::sim_exprs_snd))
            in
            new_exprs::(regroup rtl)

          else

            begin

              if is_right_distributive top_op op && ce2 < ce1 then
                let sim_exprs, rtl =
                  List.partition
                    (fun e ->
                       match e with
                       | SkBinop (op', ee, e2') when
                           op' = op && e2' @= e2 -> true
                       | _ -> false) tl
                in
                (* Extract the list of the second operands of the list of
                   binary operators *)
                let sim_exprs_fst, _ = extract_operand_lists sim_exprs in
                (* Build the new expression by lifting e2 on top at the
                   right of the operator *)
                let new_exprs =
                  SkBinop
                    (op,
                     SkApp (type_of e1, Some (get_AC_op top_op), e1::sim_exprs_fst),
                     e2)
                in
                new_exprs::(regroup rtl)

              else
                hd::(regroup tl)
            end

        | _ ->  hd::(regroup tl)
      end (** end match hd::tl *)
    | [] -> []
  in
  let applied_different_right_identities =
    List.map
      (fun op ->
         List.map
           (fun e ->
              match e with
              | SkVar v -> apply_right_identity op (type_of e) e
              | _ -> e)
           el_binops)
      operators_with_identities
  in
  let applied_different_left_identities =
    List.map
      (fun op ->
         List.map
           (fun e ->
              match e with
              | SkVar v -> apply_left_identity op (type_of e) e
              | _ -> e)
           el_binops)
      operators_with_identities
  in
  let different_factorizations =
    List.map regroup ( applied_different_left_identities @
                      applied_different_right_identities @
                      [el_binops])
  in
  let best_factorization =
    ListTools.lmin List.length different_factorizations
  in
  List.map flatten_AC (best_factorization @(el_no_binops))

let factorize ctx =
  let case e =
    match e with
    | SkApp (_, Some opvar, _) ->
      (try ignore(op_from_name opvar.vname); true with Not_found -> false)
    | _ -> false
  in
  let fact rfunc e =
    match e with
    | SkApp (t, Some opvar, el) ->
      let op = op_from_name opvar.vname in
      let fact_el = List.map rfunc (__factorize__ ctx op el) in
      SkApp (t, Some opvar, fact_el)

    | _ -> failwith "factorize_all : bad case"
  in
  transform_expr case fact identity identity


(** Transform all comparisons : Lt -> Gt Le -> Ge *)
let transform_all_comparisons expr =
  let case e =
    match e with
    | SkBinop (cop, _, _) ->
      (match cop with
       | Lt | Le -> true
       | _ -> false)
    | _ -> false
  in
  let transformer rfunc e =
    match e with
    | SkBinop (cop, e1, e2) ->
      (match cop with
       | Lt -> SkBinop (Gt, rfunc e1, rfunc e2)
       | Le -> SkBinop (Ge, rfunc e1, rfunc e2)
       | _ -> failwith "Not a special recursive case")
    | _ -> failwith "Not a special recursive case"
  in
  transform_expr case transformer identity identity expr


let __transform_conj_comps__ top_op el =
  let with_comparison, rest =
    List.partition
      (fun e ->
         match e with
         | SkBinop (op, _, _) -> List.mem op comparison_operators
         | _ -> false) el
  in
  let build_comp_conj
      (is_left_operand : bool)
      (top_op : symb_binop)
      (op : symb_binop) common_expr exprs =
    if is_left_operand then
      let _, right_op_list = extract_operand_lists exprs in
      match top_op, op with
      | Or, Lt | Or, Le | And, Gt | And, Ge  ->
        SkBinop(op,
                common_expr,
                SkApp (type_of common_expr, Some (get_AC_op Max),
                       right_op_list))

      | And, Lt | And, Le | Or, Gt | Or, Ge ->
        SkBinop(op,
                common_expr,
                SkApp (type_of common_expr, Some (get_AC_op Min),
                       right_op_list))

      | _ , _ -> failwith "Unexpected match case in build_comp_conj"
    else
      let left_op_list, _ = extract_operand_lists exprs in
      match top_op, op with
      | Or, Lt | Or, Le | And, Gt | And, Ge  ->
          SkBinop(op,
                  SkApp (type_of common_expr, Some (get_AC_op Min),
                         left_op_list),
                  common_expr)

      | And, Lt | And, Le | Or, Gt | Or, Ge ->
        SkBinop(op,
                SkApp (type_of common_expr, Some (get_AC_op Max),
                       left_op_list),
                common_expr)

      | _ , _ -> failwith "Unexpected match case in build_comp_conj"
  in
  let rec regroup el =
    match el with
    | hd :: tl ->
      begin
        match hd with
        | SkBinop (op, e1, e2) ->
          let left_common, left_tl_rest =
            List.partition
              (fun e ->
                 match e with
                 | SkBinop (op', e1', e2') -> op' = op && e1 @= e1'
                 | _ -> false)
              tl
          in
          let right_common, right_tl_rest =
            List.partition
              (fun e ->
                 match e with
                 | SkBinop (op', e1', e2') -> op' = op && e2 @= e2'
                 | _ -> false)
              tl
          in
          let hdexpr, new_tl =
            if List.length left_common > 0 || List.length right_common > 0 then
              begin
                if List.length left_common > List.length right_common
                then
                  build_comp_conj true top_op op e1 (hd::left_common),
                  left_tl_rest
                else
                  build_comp_conj false top_op op e2 (hd::right_common),
                  right_tl_rest
              end
            else
              hd, tl
          in
          hdexpr::(regroup new_tl)

        | _ -> failwith "Unexpected case in regroup el"
      end

    | [] -> []

  in
  List.map flatten_AC (rest@(regroup with_comparison))

let transform_conj_comps e =
  let case e =
    match e with
    | SkApp (_, Some vi, _) when vi.vname = "and" || vi.vname = "or" -> true
    | _ -> false
  in
  let transf rfunc e =
    match e with
    | SkApp (t, Some vi, el) ->
      let el' = List.map rfunc el in
      let op = op_from_name vi.vname in
      let new_el = __transform_conj_comps__  op el' in
      (match new_el with
       | [e] -> e
       | _ ->
         SkApp (t, Some vi, new_el))

    | _ -> failwith "Not a valid case."
  in
  transform_expr case transf identity identity e

(** Put all the special rules here *)
let apply_special_rules ctx e =
  let e' = transform_all_comparisons e in
  let e'' =
    let t_e = factorize ctx (transform_conj_comps e') in
    if cost ctx t_e < cost ctx e' then t_e else e'
  in
  factorize ctx e''

let accumulated_subexpression vi e =
  match e with
  | SkBinop (op, SkVar (SkVarinfo vi'), acc) when vi = vi' -> acc
  | SkBinop (op, acc, SkVar (SkVarinfo vi')) when vi = vi' -> acc
  | _ -> e


(** Transformations taking AC in account *)
let replace_AC ctx ~to_replace:to_replace ~by:by_expr ~ine:in_expr =
  let flat_tr = flatten_AC to_replace in
  let flat_by = flatten_AC by_expr in
  let flat_in = flatten_AC in_expr in
  let case =
    function
    | SkApp (_, Some opvar, _) ->
      (try ignore(op_from_name opvar.vname); true with Not_found -> false)
    | e when e @= flat_tr -> true
    | _ -> false
  in
  let handle_case rfunc =
    function
    | SkApp (t, Some opvar, el) ->
      let op = op_from_name opvar.vname in
      begin
        match flat_tr with
        | SkApp (t', Some opvar', el') ->
          let op' = op_from_name opvar'.vname in
          if op = op' then
            begin
              (* Split the super list in terms shared by the expression to
                 replace and other expressions *)
              let common_terms, remaining_terms =
                List.partition
                  (fun e -> List.mem e el') el
              in
              (* To have a matchimg expression all the terms in the term to
                 replace must be in the super term *)
              let common_is_el' =
                List.for_all (fun e -> List.mem e common_terms) el' in
              begin
                if common_is_el' then
                  let raw_term = SkApp (t, Some opvar, remaining_terms@[flat_by]) in
                  flatten_AC (rebuild_tree_AC ctx raw_term)
                else
                  SkApp (t, Some opvar, List.map rfunc el)
              end (* END if common_is_el' *)
            end
          else
            SkApp (t, Some opvar, List.map rfunc el)
    (* END if op = op' *)
    | _ -> SkApp (t, Some opvar, List.map rfunc el)
end
| e when e @= flat_tr -> flat_by
       | _ -> failwith "Unexpected case in replace_AC"
in
rebuild_tree_AC ctx (transform_expr case handle_case identity identity flat_in)
